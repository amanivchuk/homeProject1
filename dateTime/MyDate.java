package dateTime;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class MyDate {
    static boolean leapYear;
    private int year;
    private int month;
    private int day;

    public MyDate() throws IOException {
        this.year = Year.getYear();
        this.month = Month.getMonth();
        this.day = Day.getDay();
    }
    public void getDayOfWeek(){
        Calendar aDate = new GregorianCalendar(year,(month-1),day);
        int dayOfWeek = aDate.get(Calendar.DAY_OF_WEEK);
        System.out.println("DayOfWeek = " + DayOfWeek.valueOf(dayOfWeek));
    }
    public int daysBetween(MyDate myDate){
        String d1 = day + "." + month + "." + year;
        String d2 = myDate.day + "." + myDate.month + "." + myDate.year;
        SimpleDateFormat format = new SimpleDateFormat("dd.MM.yyyy");
        Date date1 = null;
        Date date2 = null;
        try {
            date1 = format.parse(d1);
            date2 = format.parse(d2);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        long differences = date1.getTime() - date2.getTime();
        int days =  (int)(differences / (24 * 60 * 60 * 1000));
        System.out.println("DaysBeetwen" + days);
    return days;
    }

    static class Year{
        public static int getYear() throws IOException {
            BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
            System.out.println("Enter year");
            int year = Integer.parseInt(reader.readLine());
            if ( (year %4!=0) || (year %400!=0) ) {
                System.out.println("���������� ���� � ����: 365");
                leapYear = false;
            }
            else {
                System.out.println("���������� ���� � ����: 366");
                leapYear = true;
            }
            return year;
        }
    }
    static class Month{
        public static int getMonth() throws IOException {
            int[] daysOfmonths = new int[]{31,28,31,30,31,30,31,31,30,31,30,31};
            BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
            int month;
            System.out.println("Enter month");
            int userMonth = Integer.parseInt(reader.readLine());
            if((userMonth <0) ||(userMonth > 12)){
                System.out.println("Error! Input correct month!");
                getMonth();
            }
            if(leapYear)
                daysOfmonths[1] += 1;
            month = daysOfmonths[userMonth-1];
            System.out.println("Current month has " + month + " days.");
            return userMonth;
        }
    }
    static class Day{
        public static int getDay() throws IOException {
            BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
            System.out.println("Enter day");
            int day = Integer.parseInt(reader.readLine());
            if(day < 1 || day > 31){
                System.out.println("Error. Enter correct day");
                getDay();
            }
            return day;
        }
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public int getMonth() {
        return month;
    }

    public void setMonth(int month) {
        this.month = month;
    }

    public int getDay() {
        return day;
    }

    public void setDay(int day) {
        this.day = day;
    }
}